package exceptions_exo1;

public class IncorrectCodePostalException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public IncorrectCodePostalException(String codePostal) {
		System.out.println("Le code postal : " + codePostal + " doit contenir exactement 5 chiffres");
	}

}
