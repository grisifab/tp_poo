package exceptions_exo1;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int x = 5;
		int y = 0;
		try {
		System.out.print(x/y);
		}
		catch (ArithmeticException e) { //pour les exceptions math�matiques
		// ou 	catch (Exception e) {   // pour traiter tous les types d'execptions 
			System.out.println("Exception division par z�ro");
			System.out.println("Exception " + e.getMessage()); // pour afficher le type d'exception
			e.printStackTrace(); // idem + details + ligne de code qui merde
		}
		System.out.println("Fin de calcul");
		
		//----- avec nos exceptions de nous -------
		
		//tout OK
		try {
			Adresse a1 = new Adresse( "MASSENA","Nice", "06100");
		} catch (IncorrectCodePostalException | IncorrectStreetNameException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//mauvais CP
		try {
			Adresse a2 = new Adresse("MASSENA","Nice", "6100");
		} catch (IncorrectCodePostalException | IncorrectStreetNameException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// mauvaise rue
		try {
			Adresse a2 = new Adresse("MaSSENA", "Nice", "06100");
		} catch (IncorrectCodePostalException | IncorrectStreetNameException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
