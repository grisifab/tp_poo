package exceptions_exo1;

public class Adresse {
	
	private String rue;
	private String ville;
	private String codePostal;
	
	
	public Adresse(String rue, String ville, String codePostal) throws IncorrectCodePostalException, IncorrectStreetNameException
	{
		if(codePostal.length() != 5) throw new IncorrectCodePostalException(codePostal);
		if(rue.toUpperCase() != rue) throw new IncorrectStreetNameException(rue);
		this.rue = rue;
		this.ville = ville;
		this.codePostal = codePostal;
	}

	@Override
	public String toString() {
		return "Adresse [rue=" + rue + ", ville=" + ville + ", codePostal=" + codePostal + "]";
	}

	public String getRue() {
		return rue;
	}

	public void setRue(String rue) throws IncorrectStreetNameException {
		if(rue.toUpperCase() != rue) throw new IncorrectStreetNameException(rue);
		this.rue = rue;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {		
		this.ville = ville;
	}

	public String getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(String codePostal) throws IncorrectCodePostalException {
		if(codePostal.length() != 5) throw new IncorrectCodePostalException(codePostal);
		this.codePostal = codePostal;
	}
	
	

}
