package collections_exo1;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.ListIterator;

public class Test {

	public static void main(String[] args) { 
		ArrayList liste = new ArrayList();
		liste.add(0); 
		liste.add("bonjour"); 
		liste.add(2); 
		liste.remove(1); 
		liste.set(1, "bonsoir"); 
		
		for(Object elt : liste) { 
			System.out.print(elt + " "); 
		} 
		
		System.out.println("---------------"); 
		LinkedList liste1 = new LinkedList(); 
		liste1.add(5); 
		liste1.add("Bonjour "); 
		liste1.add(7.5f);
		for(int i = 0; i < liste1.size(); i++) 
			System.out.println("element d�indice " + i + " = " + liste1.get(i));
		
		System.out.println("---------------"); 
		LinkedList liste2 = new LinkedList();
		liste2.add(5);
		liste2.add("Bonjour "); 
		liste2.add(7.5f);
		ListIterator li = liste2.listIterator();
		while(li.hasNext()) System.out.println(li.next());
		
		System.out.println("---------------"); 
		LinkedList l = new LinkedList();
		l.add(0); 
		l.add("bonjour"); 
		l.addFirst("premier"); 
		l.addLast("dernier"); 
		String s = "Salut"; 
		l.add(s); 
		int value = 2; 
		l.add(value); 
		l.remove("dernier"); 
		l.remove(s); 
		l.remove((Object)value); 
		ListIterator li2 = l.listIterator();
		while(li2.hasNext()) System.out.print(li2.next() + " ");
		
		
	}
}
