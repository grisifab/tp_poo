package collections_exo2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.ListIterator;

public class Tests {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// ---------------Array List-------------------------------
		ArrayList<Integer> maListeEntier = new ArrayList<Integer>();
		for (int i = 0; i < 10; i++) {
			maListeEntier.add(i);
		}

		System.out.println("\n m�thode1");
		for (int i = 0; i < maListeEntier.size(); i++) {
			System.out.println(maListeEntier.get(i));
		}

		System.out.println("\n m�thode2");
		for (Object elt : maListeEntier) {
			System.out.print(elt + " ");
		}

		System.out.println("\n \n recherche �l�ment mth1");
		int j = 0;
		while (maListeEntier.get(j) != 6) {
			j++;
		}
		System.out.println("La valeur 6, se trouve � l'indice " + j + " du tableau");

		System.out.println("\n recherche �l�ment mth2");
		System.out.println("La valeur 8, se trouve � l'indice " + maListeEntier.indexOf(8) + " du tableau");

		System.out.println("\n recherche �l�ment mth3");
		System.out.println("La valeur 2, se trouve � l'indice " + maListeEntier.lastIndexOf(2) + " du tableau");

		ArrayList<Integer> maListeEntier2 = new ArrayList<Integer>();
		for (int i = 0; i < 10; i++) {
			maListeEntier2.add(i + 10);
		}

		for (int i = 0; i < maListeEntier2.size(); i++) {
			maListeEntier.add(maListeEntier2.get(i));
		}

		System.out.println("\n affiche cummul de 2 arrayList");
		for (Object elt : maListeEntier) {
			System.out.print(elt + " ");
		}

		// ---------------LinkedList-------------------------------

		LinkedList<Double> lListe = new LinkedList<Double>();
		for (int i = 0; i < 12; i++) {
			lListe.add(i + 0.1);
		}

		System.out.println("\n \n Linked list");
		ListIterator<Double> li = lListe.listIterator();
		while (li.hasNext()) {
			System.out.println(li.next());
		}

		lListe.addFirst(-12.3);
		lListe.addLast(12.3);

		li = lListe.listIterator();
		while (li.hasNext()) {
			System.out.println(li.next());
		}

		lListe.add(6, 56.3);

		lListe.forEach(System.out::println);

		// -----------------------------------------
		HashSet<Integer> hsdmc = new HashSet<Integer>();
		hsdmc.add(12);
		hsdmc.add(122);
		hsdmc.add(1222);
		hsdmc.add(12222);
		
		System.out.println("le HashSet a " + hsdmc.size() + " �l�ments");
		
		hsdmc.removeAll(hsdmc);
		
		hsdmc.add(12);
		hsdmc.add(122);
		hsdmc.add(1222);
		hsdmc.add(12222);
		
		hsdmc.forEach(System.out::println);
		
		//�crivez un programme Java pour convertir un HashSet en ArrayList. 
		System.out.println(" ");
		ArrayList<Integer> maListeEntier3 = new ArrayList<Integer>(hsdmc);
		maListeEntier3.forEach(System.out::println);
		
		System.out.println("Mettre le bordel");
		Collections.shuffle(maListeEntier3);
		maListeEntier3.forEach(System.out::println);
		
		System.out.println("Trier d�croissant");
		Collections.reverse(maListeEntier3);
		maListeEntier3.forEach(System.out::println);
		
		System.out.println("Trier croissant");
		Collections.sort(maListeEntier3);
		maListeEntier3.forEach(System.out::println);
		
		System.out.println("Trier d�croissant");
		Collections.reverse(maListeEntier3);
		maListeEntier3.forEach(System.out::println);
	}

}
