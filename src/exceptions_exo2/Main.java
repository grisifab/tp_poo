package exceptions_exo2;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int x = 5;
		int y = 0;
		try {
		System.out.print(x/y);
		}
		catch (ArithmeticException e) { //pour les exceptions math�matiques
		// ou 	catch (Exception e) {   // pour traiter tous les types d'execptions 
			System.out.println("Exception division par z�ro");
			System.out.println("Exception " + e.getMessage()); // pour afficher le type d'exception
			e.printStackTrace(); // idem + details + ligne de code qui merde
		}
		System.out.println("Fin de calcul");
		
		//----- avec nos exceptions de nous -------
		
		Adresse a1,a2,a3 = new Adresse();
		
		// mauvaise rue
		try {
			a1 = new Adresse( "MASSENA","Nice", "06100");
			a1.setRue("MaSSENA");
		} catch (IncorrectAdresseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// mauvais CP	
		try {
			a2 = new Adresse("MASSENA","Nice", "06100");
			a2.setCodePostal("6100");
		} catch (IncorrectAdresseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// tout OK
		try {
			a3 = new Adresse("MASSENA", "Nice", "06100");
		} catch (IncorrectAdresseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// mauvais CP
		try {
			a2 = new Adresse("MASSENA", "Nice", "0610");
		} catch (IncorrectAdresseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
}
