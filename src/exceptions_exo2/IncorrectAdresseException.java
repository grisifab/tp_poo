package exceptions_exo2;

public class IncorrectAdresseException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public IncorrectAdresseException(String codePostal, String rue) {
		if(codePostal.length() != 5) {
			System.out.println("Le code postal : " + codePostal + " doit contenir exactement 5 chiffres");
		} else {
			System.out.println("La rue : "+ rue +" doit �tre en MAJUSCULE");
		}
	}
}
