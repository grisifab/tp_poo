package exceptions_exo2;

public class Adresse {
	
	private String rue;
	private String ville;
	private String codePostal;
	
	
	
	public Adresse() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Adresse(String rue, String ville, String codePostal) throws  IncorrectAdresseException
	{
		if((codePostal.length() != 5) | (rue.toUpperCase() != rue)) throw new  IncorrectAdresseException(codePostal,rue);
		this.rue = rue;
		this.ville = ville;
		this.codePostal = codePostal;
	}

	@Override
	public String toString() {
		return "Adresse [rue=" + rue + ", ville=" + ville + ", codePostal=" + codePostal + "]";
	}

	public String getRue() {
		return rue;
	}

	public void setRue(String rue) throws IncorrectAdresseException {
		if(rue.toUpperCase() != rue) throw new IncorrectAdresseException(codePostal,rue);
		this.rue = rue;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {		
		this.ville = ville;
	}

	public String getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(String codePostal) throws IncorrectAdresseException {
		if(codePostal.length() != 5) throw new IncorrectAdresseException(codePostal,rue);
		this.codePostal = codePostal;
	}
	
	

}
