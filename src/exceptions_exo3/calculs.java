package exceptions_exo3;

import java.util.InputMismatchException;
import java.util.Scanner;

public class calculs {
	
	public static int saisieCorrecte() throws IncorrectValue {
		int num1 = 0;
		boolean ok;
		Scanner monScanner = new Scanner(System.in);
		
		do {
			ok = true;
			System.out.println("Saisir un entier >= 10");
			try {
				num1 = monScanner.nextInt();
				if(num1 < 10) throw new IncorrectValue();
			}
			catch (InputMismatchException e) {
				System.out.println("Ceci n'est pas un entier");
				ok = false;
			}			
			monScanner.nextLine();
		}while(!ok);
		
		monScanner.close();
		return num1;
	}

}
