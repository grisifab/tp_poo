package laBase;

import java.util.ArrayList;
import java.util.List;

public class Bureau {
	
	private int code;
	private String nomb;
	private List<Chercheur> lesChercheurs = new ArrayList<Chercheur>();
	
	
	public Bureau(int code, String nomb, List<Chercheur> lesChercheurs) {
		super();
		this.code = code;
		this.nomb = nomb;
		this.lesChercheurs = lesChercheurs;
	}

	@Override
	public String toString() {
		return "Bureau [code=" + code + ", nomb=" + nomb + ", lesChercheurs=" + lesChercheurs + "]";
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getNomb() {
		return nomb;
	}

	public void setNomb(String nomb) {
		this.nomb = nomb;
	}

	public List<Chercheur> getLesChercheurs() {
		return lesChercheurs;
	}

	public void setLesChercheurs(List<Chercheur> lesChercheurs) {
		this.lesChercheurs = lesChercheurs;
	}

	public Bureau() {
		super();
		// TODO Auto-generated constructor stub
	}	
	

}
