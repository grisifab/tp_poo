package laBase;

import java.util.ArrayList;
import java.util.List;

public class Laboratoire {
	
	private String name;
	private String specialite;
	private Adresse adresse;
	private List<Bureau> lesBureaux = new ArrayList<Bureau>();
	
	public Laboratoire(String name, String specialite, Adresse adresse, List<Bureau> lesBureaux) {
		super();
		this.name = name;
		this.specialite = specialite;
		this.adresse = adresse;
		this.lesBureaux = lesBureaux;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSpecialite() {
		return specialite;
	}

	public void setSpecialite(String specialite) {
		this.specialite = specialite;
	}

	public Adresse getAdresse() {
		return adresse;
	}

	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}

	public List<Bureau> getLesBureaux() {
		return lesBureaux;
	}

	public void setLesBureaux(List<Bureau> lesBureaux) {
		this.lesBureaux = lesBureaux;
	}

	@Override
	public String toString() {
		return "Laboratoire [name=" + name + ", specialite=" + specialite + ", adresse=" + adresse + ", lesBureaux="
				+ lesBureaux + "]";
	}

}
