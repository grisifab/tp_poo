package laBase;

public class Chercheur {
	
	private int idc;
	private String nom;
	private String poste;
	private int numOrdi;
	private int count = 1;
	
	public Chercheur(String nom, String poste, int numOrdi) {
		super();
		this.nom = nom;
		this.poste = poste;
		this.numOrdi = numOrdi;
		this.idc = count++;
	}
	
	public int getNbInstChercheur(){return count;}
	
	public void comparer(Chercheur chx) {
		if((this.getNom() == chx.getNom())&&(this.getPoste() == chx.getPoste())&&(this.getNumOrdi() == chx.getNumOrdi())) {
			System.out.println("Chercheurs identiques");
		} else {
			System.out.println("Chercheurs différents");
		}
	} 

	public Chercheur() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Chercheur [nom=" + nom + ", poste=" + poste + ", numOrdi=" + numOrdi + "]";
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPoste() {
		return poste;
	}

	public void setPoste(String poste) {
		this.poste = poste;
	}

	public int getNumOrdi() {
		return numOrdi;
	}

	public void setNumOrdi(int numOrdi) {
		this.numOrdi = numOrdi;
	}
	
	

}
