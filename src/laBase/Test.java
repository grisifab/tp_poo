package laBase;

import java.util.ArrayList;
import java.util.List;

public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//----------Adresse
		Adresse adr1 = new Adresse("rue de la r�pulique", "Valbonne", "06560");	
		System.out.println(adr1);
		
		adr1.setCodePostal("06300");
		adr1.setRue("chemin du Conteo");
		adr1.setVille("Nissa");
		System.out.println(adr1);
		
		//----------Chercheur
		Chercheur ch1 = new Chercheur("Raoul Volfoni", "Trouveur", 123);
		System.out.println(ch1);
		
		ch1.setNom("Didier l'embrouille");
		ch1.setNumOrdi(456);
		ch1.setPoste("placard");
		System.out.println(ch1);
		
		Chercheur ch2 = new Chercheur();
		System.out.println(ch2);
		
		System.out.println(ch1.getNbInstChercheur()); // marche avec n'importe quel chercheur
		ch1.comparer(ch2);
		ch1.comparer(ch1);
		
		ch2.setNumOrdi(123);
		
		Chercheur ch3 = new Chercheur();
		ch3.setNom("Eva Prendrecher");
		ch3.setNumOrdi(486);
		ch3.setPoste("Laborantine");
		System.out.println(ch3);
		
		//----------bureaux
		
		Chercheur ch4 = new Chercheur();
		Chercheur ch5 = new Chercheur();
		Chercheur ch6 = new Chercheur();
		
		List<Chercheur> lesChercheurs1 = new ArrayList<Chercheur>();
		lesChercheurs1.add(ch1);
		lesChercheurs1.add(ch2);
		lesChercheurs1.add(ch3);
		
		List<Chercheur> lesChercheurs2 = new ArrayList<Chercheur>();
		lesChercheurs2.add(ch4);
		lesChercheurs2.add(ch5);
		lesChercheurs2.add(ch6);
		
		Bureau b1 = new Bureau(1, "Chimie", lesChercheurs1);
		Bureau b2 = new Bureau(2, "M�canique", lesChercheurs2);
		
		System.out.println(b1);
		
		//----------laboratoire
		
		List<Bureau> lesBureaux1 = new ArrayList<Bureau>();
		lesBureaux1.add(b1);
		lesBureaux1.add(b2);
		
		Laboratoire l1 = new Laboratoire("Machin", "Chimie et m�ca", adr1, lesBureaux1);
		System.out.println(l1);
	}

}
